//! An implementation of the Stateless OpenPGP Command Line Interface
//! using RNP.
//!
//! This implements a subset of the [Stateless OpenPGP Command Line
//! Interface] using the GPGME OpenPGP implementation.
//!
//!   [Stateless OpenPGP Command Line Interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

use rnp_sop::RNP;

fn main() {
    sop::cli::main(&RNP::default());
}
