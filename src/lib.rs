//! An implementation of the Stateless OpenPGP Command Line Interface
//! using RNP.
//!
//! This implements a subset of the [Stateless OpenPGP Command Line
//! Interface] using the RNP OpenPGP implementation.
//!
//!   [Stateless OpenPGP Command Line Interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

use std::{
    ptr::{null, null_mut},
    ffi::{
        CStr,
        CString,
    },
    io,
    time::{UNIX_EPOCH, Duration, SystemTime},
};

use libc::{
    c_char,
    c_void,
    size_t,
};

use librnp_sys::*;

use sop::*;
use sop::Result;

const GPG: *const c_char = b"GPG\0".as_ptr() as *const _;
const FINGERPRINT: *const c_char = b"fingerprint\0".as_ptr() as *const _;
const SHA512: *const c_char = b"SHA512\0".as_ptr() as *const _;

pub struct Certs<'s> {
    #[allow(dead_code)]
    rnp: &'s RNP,
    data: Vec<u8>,
}

impl Certs<'_> {
    /// Loads the certs into the given FFI Context.
    fn load(&self, ffi: &FFI) -> Result<Vec<rnp_key_handle_t>> {
        unsafe {
            _load_keys(ffi.0, &self.data[..], RNP_LOAD_SAVE_PUBLIC_KEYS)
        }
    }
}

impl<'s> sop::Import<'s, RNP> for Certs<'s> {
    fn import(rnp: &'s RNP, source: &mut (dyn io::Read + Send + Sync))
              -> Result<Self>
    where
        Self: Sized,
    {
        let mut data = vec![];
        source.read_to_end(&mut data)?;
        Ok(Certs { rnp, data, })
    }
}

impl sop::Export for Certs<'_> {
    fn export(&self, armored: bool, sink: &mut (dyn io::Write + Send + Sync))
              -> Result<()> {
        if ! armored {
            todo!()
        } else {
            sink.write_all(&self.data)?;
        }
        Ok(())
    }
}

pub struct Keys<'s> {
    #[allow(dead_code)]
    rnp: &'s RNP,
    data: Vec<u8>,
}

impl Keys<'_> {
    /// Loads the keys into the given FFI Context.
    fn load(&self, ffi: &FFI) -> Result<Vec<rnp_key_handle_t>> {
        unsafe {
            _load_keys(ffi.0, &self.data[..], RNP_LOAD_SAVE_SECRET_KEYS)
        }
    }
}

impl<'s> sop::Import<'s, RNP> for Keys<'s> {
    fn import(rnp: &'s RNP, source: &mut (dyn io::Read + Send + Sync))
              -> Result<Self>
    where
        Self: Sized,
    {
        let mut data = vec![];
        source.read_to_end(&mut data)?;
        Ok(Keys { rnp, data, })
    }
}

impl sop::Export for Keys<'_> {
    fn export(&self, armored: bool, sink: &mut (dyn io::Write + Send + Sync))
              -> Result<()> {
        if ! armored {
            todo!()
        } else {
            sink.write_all(&self.data)?;
        }
        Ok(())
    }
}

struct FFI(rnp_ffi_t);

impl FFI {
    fn new() -> Result<FFI> {
        let mut ffi = null_mut();
        rnp_try!(rnp_ffi_create(&mut ffi as *mut _, GPG, GPG));
        Ok(FFI(ffi))
    }

    fn as_ptr(&self) -> rnp_ffi_t {
        self.0
    }
}

impl Drop for FFI {
    fn drop(&mut self) {
        let _ = unsafe { rnp_ffi_destroy(self.as_ptr()) };
    }
}

#[derive(Default)]
pub struct RNP {
}

impl RNP {
    fn context(&self) -> Result<FFI> {
        FFI::new()
    }
}

impl<'s> sop::SOP<'s> for RNP {
    type Keys = Keys<'s>;
    type Certs = Certs<'s>;

    fn version(&'s self) -> Result<Box<dyn sop::Version + 's>> {
        Version::new(self)
    }
    fn generate_key(&'s self)
        -> Result<Box<dyn sop::GenerateKey<RNP, Keys> + 's>>
    {
        GenerateKey::new(self)
    }
    fn change_key_password(&'s self)
        -> Result<Box<dyn sop::ChangeKeyPassword<RNP, Keys> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn revoke_key(&'s self)
        -> Result<Box<dyn sop::RevokeKey<RNP, Certs<'s>, Keys> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn extract_cert(&'s self)
        -> Result<Box<dyn sop::ExtractCert<RNP, Certs, Keys> + 's>>
    {
        ExtractCert::new(self)
    }
    fn sign(&'s self)
        -> Result<Box<dyn sop::Sign<RNP, Keys> + 's>>
    {
        Sign::new(self)
    }
    fn verify(&'s self)
        -> Result<Box<dyn sop::Verify<RNP, Certs> + 's>>
    {
        Verify::new(self)
    }
    fn encrypt(&'s self)
        -> Result<Box<dyn sop::Encrypt<RNP, Certs, Keys> + 's>> {
        Encrypt::new(self)
    }
    fn decrypt(&'s self)
        -> Result<Box<dyn sop::Decrypt<RNP, Certs, Keys> + 's>> {
        Decrypt::new(self)
    }
    fn armor(&'s self) -> Result<Box<dyn sop::Armor + 's>> {
        Err(Error::NotImplemented)
    }
    fn dearmor(&'s self) -> Result<Box<dyn sop::Dearmor + 's>> {
        Err(Error::NotImplemented)
    }
    fn inline_detach(&'s self)
        -> Result<Box<dyn sop::InlineDetach + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn inline_verify(&'s self)
        -> Result<Box<dyn sop::InlineVerify<RNP, Certs> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn inline_sign(&'s self)
        -> Result<Box<dyn sop::InlineSign<RNP, Keys> + 's>>
    {
        Err(Error::NotImplemented)
    }
}

struct Version {
}

impl<'a> Version {
    fn new(_rnp: &'a RNP) -> Result<Box<dyn sop::Version<'a> + 'a>> {
        Ok(Box::new(Self {
        }))
    }
}

impl<'a> sop::Version<'a> for Version {
    fn frontend(&self) -> Result<sop::VersionInfo> {
        Ok(sop::VersionInfo {
            name: "rnp-sop".into(),
            version: env!("CARGO_PKG_VERSION").into(),
        })
    }

    fn backend(&self) -> Result<sop::VersionInfo> {
        Ok(sop::VersionInfo {
            name: "rnp".into(),
            version: unsafe { CStr::from_ptr(rnp_version_string_full()) }
                .to_str().unwrap().into(),
        })
    }

    fn extended(&self) -> Result<String> {
        Ok(self.backend()?.to_string())
    }
}

struct GenerateKey<'s> {
    rnp: &'s RNP,
    signing_only: bool,
    with_key_password: Option<Password>,
    userids: Vec<String>,
}

impl<'s> GenerateKey<'s> {
    fn new(rnp: &'s RNP)
           -> Result<Box<dyn sop::GenerateKey<'s, RNP, Keys<'s>> + 's>>
    {
        Ok(Box::new(Self {
            rnp,
            signing_only: false,
            with_key_password: None,
            userids: Default::default(),
        }))
    }
}

impl<'s> sop::GenerateKey<'s, RNP, Keys<'s>> for GenerateKey<'s> {
    fn signing_only(mut self: Box<Self>)
                    -> Box<dyn sop::GenerateKey<'s, RNP, Keys<'s>> + 's> {
        self.signing_only = true;
        self
    }

    fn userid(mut self: Box<Self>, userid: &str)
              -> Box<dyn sop::GenerateKey<'s, RNP, Keys<'s>> + 's> {
        self.userids.push(userid.into());
        self
    }

    fn with_key_password(mut self: Box<Self>, password: Password)
                         -> Result<Box<dyn sop::GenerateKey<'s, RNP, Keys<'s>> + 's>> {
        self.with_key_password = Some(password);
        Ok(self)
    }

    fn generate(self: Box<Self>) -> Result<Keys<'s>> {
        let ffi = self.rnp.context()?;
        let mut op = null_mut();

        // Generate the primary key.
        rnp_try!(rnp_op_generate_create(&mut op as *mut _,
                                        ffi.as_ptr(),
                                        b"rsa\0".as_ptr() as *const _));

        for u in &self.userids {
            let u = CString::new(u.as_bytes()).expect("utf8");
            rnp_try!(rnp_op_generate_set_userid(
                op as *mut _, u.as_ptr()));
        }

        rnp_try!(rnp_op_generate_execute(op));
        let mut primary = null_mut();
        rnp_try!(rnp_op_generate_get_key(op,
                                         &mut primary as *mut _));
        rnp_try!(rnp_op_generate_destroy(op));

        // Generate the subkey.
        rnp_try!(rnp_op_generate_subkey_create(&mut op as *mut _,
                                               ffi.as_ptr(),
                                               primary,
                                               b"rsa\0".as_ptr() as *const _));

        rnp_try!(rnp_op_generate_execute(op));
        rnp_try!(rnp_op_generate_destroy(op));

        let mut data = vec![];
        {
            let (sink, _sink_to) = unsafe { rnp_sink(&mut data) }?;
            rnp_try!(rnp_key_export(primary, sink,
                                    RNP_KEY_EXPORT_SECRET
                                    | RNP_KEY_EXPORT_SUBKEYS
                                    | RNP_KEY_EXPORT_ARMORED));
            rnp_try!(rnp_output_destroy(sink));
        }
        Ok(Keys { rnp: self.rnp, data, })
    }
}

struct ExtractCert<'s> {
    rnp: &'s RNP,
}

impl<'s> ExtractCert<'s> {
    fn new(rnp: &'s RNP)
           -> Result<Box<dyn sop::ExtractCert<'s, RNP, Certs, Keys<'s>> + 's>>
    {
        Ok(Box::new(Self {
            rnp,
        }))
    }
}

impl<'s> sop::ExtractCert<'s, RNP, Certs<'s>, Keys<'s>> for ExtractCert<'s> {
    fn keys(self: Box<Self>, keys: &Keys) -> Result<Certs<'s>> {
        let ffi = self.rnp.context()?;
        {
            let input = unsafe { rnp_input_bytes_ref(&keys.data)? };
            rnp_try!(rnp_load_keys(ffi.as_ptr(), GPG, input,
                                   RNP_LOAD_SAVE_PUBLIC_KEYS));
            rnp_try!(rnp_input_destroy(input));
        }

        let mut data = vec![];
        {
            let (sink, _sink_to) = unsafe { rnp_sink(&mut data) }?;
            let armorer = unsafe { rnp_armor(sink, "public key")? };
            rnp_try!(rnp_save_keys(ffi.as_ptr(), GPG, armorer,
                                   RNP_LOAD_SAVE_PUBLIC_KEYS));
            rnp_try!(rnp_output_finish(armorer));
            rnp_try!(rnp_output_destroy(armorer));
            rnp_try!(rnp_output_destroy(sink));
        }
        Ok(Certs { rnp: self.rnp, data, })
    }
}

struct Sign<'s> {
    #[allow(dead_code)]
    rnp: &'s RNP,
    ffi: FFI,
    no_armor: bool,
    mode: SignAs,
    with_key_passwords: Vec<Password>,
    signers: Vec<rnp_key_handle_t>,
}

impl<'s> Sign<'s> {
    fn new(rnp: &'s RNP) -> Result<Box<dyn sop::Sign<'s, RNP, Keys<'s>> + 's>> {
        let ffi = rnp.context()?;
        Ok(Box::new(Self {
            rnp,
            ffi,
            no_armor: false,
            mode: SignAs::Binary,
            with_key_passwords: Vec::new(),
            signers: vec![],
        }))
    }
}

impl<'s> sop::Sign<'s, RNP, Keys<'s>> for Sign<'s> {
    fn no_armor(mut self: Box<Self>)
                -> Box<dyn sop::Sign<'s, RNP, Keys<'s>> + 's>
    {
        self.no_armor = true;
        self
    }

    fn mode(mut self: Box<Self>, mode: SignAs)
            -> Box<dyn sop::Sign<'s, RNP, Keys<'s>> + 's>
    {
        self.mode = mode;
        self
    }

    fn with_key_password(mut self: Box<Self>, password: Password)
              -> Result<Box<dyn sop::Sign<'s, RNP, Keys<'s>> + 's>> {
        self.with_key_passwords.push(password);
        Ok(self)
    }

    fn keys(mut self: Box<Self>, keys: &Keys)
            -> Result<Box<dyn sop::Sign<'s, RNP, Keys<'s>> + 's>> {
        self.signers.append(&mut keys.load(&self.ffi)?);
        Ok(self)
    }

    fn data<'d>(self: Box<Self>, data: &'d mut (dyn io::Read + Send + Sync))
                -> Result<Box<dyn ReadyWithResult<sop::Micalg> + 'd>>
    where
        's: 'd,
    {
        Ok(Box::new(SignReady {
            sign: *self,
            data,
        }))
    }
}

struct SignReady<'s> {
    sign: Sign<'s>,
    data: &'s mut (dyn io::Read + Send + Sync),
}

impl<'s> sop::ReadyWithResult<sop::Micalg> for SignReady<'s> {
    fn write_to(self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<sop::Micalg>
    {
        match self.sign.mode {
            SignAs::Binary => (),
            _ => return Err(Error::UnsupportedOption.into()),
        }

        unsafe {
            let (input, _input_to) = rnp_source(self.data)?;
            let (output, _output_to) = rnp_sink(sink)?;
            let mut op = null_mut();
            rnp_try!(rnp_op_sign_detached_create(&mut op as *mut _,
                                                 self.sign.ffi.as_ptr(),
                                                 input, output));
            rnp_try!(rnp_op_sign_set_armor(op, ! self.sign.no_armor));
            for key in &self.sign.signers {
                let mut sig = null_mut();
                rnp_try!(rnp_op_sign_add_signature(op, *key, &mut sig as *mut _));

                // Fix the hash algorithm so that we can return the micalg
                // value.  Pick SHA2-512 because it is the fastest hash
                // algorithm on 64 bit machines.
                rnp_try!(rnp_op_sign_signature_set_hash(sig, SHA512));

                // No need to free sig.
            }
            let _password_provider =
                PasswordProvider::install(&self.sign.ffi,
                                          &self.sign.with_key_passwords)?;

            rnp_try!(rnp_op_sign_execute(op));

            rnp_try!(rnp_op_sign_destroy(op));
            rnp_try!(rnp_input_destroy(input));
            rnp_try!(rnp_output_destroy(output));
        }

        for key in self.sign.signers {
            rnp_try!(rnp_key_handle_destroy(key));
        }

        Ok(sop::Micalg::SHA512)
    }
}

struct Verify<'s> {
    #[allow(dead_code)]
    rnp: &'s RNP,
    ffi: FFI,
    not_before: Option<SystemTime>,
    not_after: Option<SystemTime>,
}

impl<'s> Verify<'s> {
    fn new(rnp: &'s RNP)
           -> Result<Box<dyn sop::Verify<'s, RNP, Certs<'s>> + 's>>
    {
        let ffi = rnp.context()?;

        Ok(Box::new(Self {
            rnp,
            ffi,
            not_before: None,
            not_after: None,
        }))
    }
}

impl<'s> sop::Verify<'s, RNP, Certs<'s>> for Verify<'s> {
    fn not_before(mut self: Box<Self>, t: SystemTime)
                  -> Box<dyn sop::Verify<'s, RNP, Certs<'s>> + 's> {
        self.not_before = Some(t);
        self
    }

    fn not_after(mut self: Box<Self>, t: SystemTime)
                 -> Box<dyn sop::Verify<'s, RNP, Certs<'s>> + 's> {
        self.not_after = Some(t);
        self
    }

    fn certs(self: Box<Self>, certs: &Certs)
            -> Result<Box<dyn sop::Verify<'s, RNP, Certs<'s>> + 's>> {
        for cert in certs.load(&self.ffi)? {
            // We don't need the handles.
            rnp_try!(rnp_key_handle_destroy(cert));
        }
        Ok(self)
    }

    fn signatures<'d>(self: Box<Self>,
                      signatures: &'d mut (dyn io::Read + Send + Sync))
                      -> Result<Box<dyn sop::VerifySignatures + 'd>>
    where
        's: 'd,
    {
        Ok(Box::new(VerifySignatures {
            verify: *self,
            signatures: signatures,
        }))
    }
}

struct VerifySignatures<'s> {
    verify: Verify<'s>,
    signatures: &'s mut (dyn io::Read + Send + Sync),
}

impl sop::VerifySignatures for VerifySignatures<'_> {
    fn data(self: Box<Self>, data: &mut (dyn io::Read + Send + Sync))
            -> Result<Vec<sop::Verification>>
    {
        let (signatures, _signatures_to) =
            unsafe { rnp_source(self.signatures)? };
        let (input, _input_to) = unsafe { rnp_source(data)? };

        let mut op = null_mut();
        rnp_try!(rnp_op_verify_detached_create(
            &mut op as *mut _, self.verify.ffi.as_ptr(), input, signatures));
        rnp_try!(rnp_op_verify_execute(op));
        rnp_try!(rnp_input_destroy(signatures));
        rnp_try!(rnp_input_destroy(input));

        let result = sigs2verifications(
            op, self.verify.not_before, self.verify.not_after);
        rnp_try!(rnp_op_verify_destroy(op));
        result
    }
}

/// Converts `gpgme::Signature`s to `sop::Verification`s.
fn sigs2verifications<'s>(op: rnp_op_verify_t,
                          not_before: Option<SystemTime>,
                          not_after: Option<SystemTime>)
                          -> sop::Result<Vec<sop::Verification>> {
    let mut verifications = Vec::new();

    // Iterate over the signatures.
    let mut count = 0;
    rnp_try!(rnp_op_verify_get_signature_count(
        op, &mut count as *mut _));
    for i in 0..count {
        let mut sig = null_mut();
        rnp_try!(rnp_op_verify_get_signature_at(
            op, i, &mut sig as *mut _));

        // Check the signature.
        let status = unsafe {
            rnp_op_verify_signature_get_status(sig)
        };
        if status != RNP_SUCCESS {
            continue;
        }

        // Handle signature creation times.
        let mut creation_time = 0u32;
        rnp_try!(rnp_op_verify_signature_get_times(
            sig, &mut creation_time as *mut _, null_mut()));
        let creation_time =
            UNIX_EPOCH + Duration::new(creation_time as u64, 0);

        if let Some(not_before) = not_before {
            if creation_time < not_before {
                continue;
            }
        }

        if let Some(not_after) = not_after {
            if creation_time > not_after {
                continue;
            }
        }

        // Get fingerprints.
        let mut handle = null_mut();
        rnp_try!(rnp_op_verify_signature_get_key(
            sig, &mut handle as *mut _));

        let mut key_raw = null_mut();
        rnp_try!(rnp_key_get_fprint(
            handle, &mut key_raw as *mut _));
        let key = unsafe { CStr::from_ptr(key_raw) }
            .to_str().expect("utf8").to_string();
        unsafe { rnp_buffer_destroy(key_raw as *mut _) };

        // Get the cert's fingerprint.  Note that we must not
        // call rnp_key_get_primary_fprint if handle refers to
        // the primary key fingerprint already.
        let mut is_primary = false;
        rnp_try!(rnp_key_is_primary(handle, &mut is_primary));
        let cert = if is_primary {
            key.clone()
        } else {
            let mut cert_raw = null_mut();
            rnp_try!(rnp_key_get_primary_fprint(
                handle, &mut cert_raw as *mut _));
            let cert = unsafe { CStr::from_ptr(cert_raw) }
                .to_str().expect("utf8").to_string();
            unsafe { rnp_buffer_destroy(cert_raw as *mut _) };
            cert
        };

        rnp_try!(rnp_key_handle_destroy(handle));

        // Finally, report.
        verifications.push(Verification::new(
            creation_time,
            key,
            cert,
            sop::SignatureMode::Binary, // XXX: RNP doesn't provide this.
            None,
        )?);
    }

    Ok(verifications)
}

struct Encrypt<'s> {
    #[allow(dead_code)]
    rnp: &'s RNP,
    ffi: FFI,
    no_armor: bool,
    mode: EncryptAs,
    passwords: Vec<Password>,
    with_key_passwords: Vec<Password>,
    signers: Vec<rnp_key_handle_t>,
    recipients: Vec<rnp_key_handle_t>,
}

impl<'s> Encrypt<'s> {
    fn new(rnp: &'s RNP)
           -> Result<Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        let ffi = rnp.context()?;
        Ok(Box::new(Self {
            rnp,
            ffi,
            no_armor: false,
            mode: EncryptAs::Binary,
            passwords: Vec::new(),
            with_key_passwords: Vec::new(),
            signers: Vec::new(),
            recipients: Vec::new(),
        }))
    }
}

impl<'s> sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> for Encrypt<'s> {
    fn no_armor(mut self: Box<Self>)
                -> Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>
    {
        self.no_armor = true;
        self
    }

    fn mode(mut self: Box<Self>, mode: EncryptAs)
            -> Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>
    {
        self.mode = mode;
        self
    }

    fn sign_with_keys(mut self: Box<Self>,
                      keys: &Keys)
                      -> Result<Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        self.signers.append(&mut keys.load(&self.ffi)?);
        Ok(self)
    }

    fn with_key_password(mut self: Box<Self>, password: Password)
                         -> Result<Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        self.with_key_passwords.push(password);
        Ok(self)
    }

    fn with_password(mut self: Box<Self>, password: Password)
                     -> Result<Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        self.passwords.push(password);
        Ok(self)
    }

    fn with_certs(mut self: Box<Self>, certs: &Certs)
                  -> Result<Box<dyn sop::Encrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        self.recipients.append(&mut certs.load(&self.ffi)?);
        Ok(self)
    }

    fn plaintext<'d>(self: Box<Self>,
                     plaintext: &'d mut (dyn io::Read + Send + Sync))
                     -> Result<Box<dyn ReadyWithResult<Option<sop::SessionKey>> + 'd>>
    where
        's: 'd,
    {
        Ok(Box::new(EncryptReady {
            encrypt: *self,
            plaintext,
        }))
    }
}

struct EncryptReady<'s> {
    encrypt: Encrypt<'s>,
    plaintext: &'s mut (dyn io::Read + Send + Sync),
}

impl<'s> sop::ReadyWithResult<Option<sop::SessionKey>> for EncryptReady<'s> {
    fn write_to(self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<Option<sop::SessionKey>>
    {
        match self.encrypt.mode {
            EncryptAs::Binary => (),
            _ => return Err(Error::UnsupportedOption.into()),
        }

        let (input, _input_to) = unsafe { rnp_source(self.plaintext)? };
        let (output, _output_to) = unsafe { rnp_sink(sink)? };
        let mut op = null_mut();
        rnp_try!(rnp_op_encrypt_create(&mut op as *mut _,
                                       self.encrypt.ffi.as_ptr(),
                                       input, output));
        rnp_try!(rnp_op_encrypt_set_armor(op, ! self.encrypt.no_armor));

        for cert in &self.encrypt.recipients {
            rnp_try!(rnp_op_encrypt_add_recipient(op, *cert));
        }

        for key in &self.encrypt.signers {
            rnp_try!(rnp_op_encrypt_add_signature(op, *key, null_mut()));
        }
        let _password_provider =
            PasswordProvider::install(&self.encrypt.ffi,
                                      &self.encrypt.with_key_passwords)?;

        for p in self.encrypt.passwords.into_iter()
            .map(|p| CString::new(p.normalized()).expect("normalized"))
        {
            rnp_try!(rnp_op_encrypt_add_password(
                op, p.as_ptr(), null(), 0, null()));
        }

        rnp_try!(rnp_op_encrypt_execute(op));
        rnp_try!(rnp_op_encrypt_destroy(op));
        rnp_try!(rnp_input_destroy(input));
        rnp_try!(rnp_output_destroy(output));
        for cert in self.encrypt.recipients {
            rnp_try!(rnp_key_handle_destroy(cert));
        }
        for key in self.encrypt.signers {
            rnp_try!(rnp_key_handle_destroy(key));
        }

        Ok(None)
    }
}

struct Decrypt<'s> {
    #[allow(dead_code)]
    rnp: &'s RNP,
    ffi: FFI,
    not_before: Option<SystemTime>,
    not_after: Option<SystemTime>,
    passwords: Vec<Password>,
}

impl<'s> Decrypt<'s> {
    fn new(rnp: &'s RNP)
           -> Result<Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        let ffi = rnp.context()?;
        Ok(Box::new(Self {
            rnp,
            ffi,
            not_before: None,
            not_after: None,
            passwords: Vec::new(),
        }))
    }
}

impl<'s> sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> for Decrypt<'s> {
    /// Makes SOP consider signatures before this date invalid.
    fn verify_not_before(mut self: Box<Self>, t: SystemTime)
                         -> Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>
    {
        self.not_before = Some(t);
        self
    }

    /// Makes SOP consider signatures after this date invalid.
    fn verify_not_after(mut self: Box<Self>, t: SystemTime)
                        -> Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>
    {
        self.not_after = Some(t);
        self
    }

    fn verify_with_certs(self: Box<Self>,
                         certs: &Certs)
                         -> Result<Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        // We don't need the handles.
        for cert in certs.load(&self.ffi)? {
            rnp_try!(rnp_key_handle_destroy(cert));
        }
        Ok(self)
    }

    /// Tries to decrypt with the given session key.
    fn with_session_key(self: Box<Self>, _sk: sop::SessionKey)
                        -> Result<Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        Err(Error::NotImplemented)
    }

    /// Tries to decrypt with the given password.
    fn with_password(mut self: Box<Self>, password: Password)
                     -> Result<Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        self.passwords.push(password);
        Ok(self)
    }

    fn with_keys(self: Box<Self>, keys: &Keys)
                 -> Result<Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        // We don't need the handles.
        for key in keys.load(&self.ffi)? {
            rnp_try!(rnp_key_handle_destroy(key));
        }
        Ok(self)
    }

    fn with_key_password(mut self: Box<Self>, password: Password)
                         -> Result<Box<dyn sop::Decrypt<'s, RNP, Certs<'s>, Keys<'s>> + 's>>
    {
        // Note: RNP doesn't care whether it is for keys or
        // ciphertexts, the mechanism is the same.
        self.passwords.push(password);
        Ok(self)
    }

    /// Decrypts `ciphertext`, returning verification results and
    /// plaintext.
    fn ciphertext<'d>(self: Box<Self>,
                      ciphertext: &'d mut (dyn io::Read + Send + Sync))
                      -> Result<Box<dyn sop::ReadyWithResult<(Option<sop::SessionKey>,
                                                              Vec<sop::Verification>)> + 'd>>
    where
        's: 'd,
    {
        Ok(Box::new(DecryptReady {
            decrypt: *self,
            ciphertext,
        }))
    }
}

struct DecryptReady<'s> {
    decrypt: Decrypt<'s>,
    ciphertext: &'s mut (dyn io::Read + Send + Sync),
}

impl<'s> sop::ReadyWithResult<(Option<sop::SessionKey>, Vec<sop::Verification>)>
    for DecryptReady<'s>
{
    fn write_to(self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<(Option<sop::SessionKey>, Vec<sop::Verification>)> {
        let _password_provider =
            PasswordProvider::install(&self.decrypt.ffi, &self.decrypt.passwords)?;

        let (input, _input_to) = unsafe { rnp_source(self.ciphertext)? };
        let (output, _output_to) = unsafe { rnp_sink(sink)? };

        let mut op = null_mut();
        rnp_try!(rnp_op_verify_create(
            &mut op as *mut _, self.decrypt.ffi.as_ptr(), input, output));
        rnp_try!(rnp_op_verify_execute(op));

        let verifications = sigs2verifications(
            op, self.decrypt.not_before, self.decrypt.not_after)?;
        rnp_try!(rnp_op_verify_destroy(op));

        Ok((None, verifications))
    }
}

/// Feeds passwords for decrypting keys and ciphertexts to RNP.
struct PasswordProvider {
    /// The passwords to try in expanded form (i.e. all variants).
    expanded_passwords: Vec<Vec<u8>>,

    /// Per-object cursor to keep track of which passwords we tried.
    cursors: std::collections::BTreeMap<String, usize>,
}

impl PasswordProvider {
    /// Installs the password provider.
    ///
    /// ATTENTION: The PasswordProvider object must be kept
    /// alive as long as `ffi`.
    fn install(ffi: &FFI, passwords: &[Password]) -> Result<Box<Self>> {
        /// Feeds passwords to RNP.
        ///
        /// ATTENTION: Cookie must be a &mut Vec<Vec<u8>>.
        unsafe extern "C"
        fn password_cb(_: rnp_ffi_t,
                       cookie: *mut c_void,
                       key: rnp_key_handle_t,
                       context: *const c_char,
                       buf: *mut c_char,
                       len: size_t)
                       -> bool {
            let provider: &mut PasswordProvider = std::mem::transmute(cookie);

            // We need to try every password for every possible
            // object.  For that, we keep track of which ones we tried
            // for what in the cursors map.
            let cursor_key = unsafe {
                let context = CStr::from_ptr(context);
                if key.is_null() {
                    context.to_str().expect("utf8").to_string()
                } else {
                    let mut fp_raw = null_mut();
                    if rnp_key_get_fprint(
                        key, &mut fp_raw as *mut _) == RNP_SUCCESS
                    {
                        let r = format!(
                            "{}:{}",
                            context.to_str().expect("utf8"),
                            CStr::from_ptr(fp_raw).to_str().expect("utf8"));
                        rnp_buffer_destroy(fp_raw as *mut _);
                        r
                    } else {
                        format!(
                            "{}:extracting fingerprint failed",
                            context.to_str().expect("utf8"))
                    }
                }
            };

            let i = *provider.cursors.entry(cursor_key)
                .and_modify(|i| *i += 1)
                .or_insert(0);
            if let Some(p) = provider.expanded_passwords.get(i) {
                // If p plus the terminating zero doesn't fit buf,
                // tough luck.  This is a limitation of the RNP API.
                let buf =
                    std::slice::from_raw_parts_mut(buf as *mut u8, len);
                let l = (buf.len() - 1).min(p.len());
                buf[..l].copy_from_slice(&p[..l]);
                // Terminate string.
                buf[l] = 0;
                true
            } else {
                false
            }
        }

        let mut provider = Box::new(PasswordProvider {
            expanded_passwords: passwords.iter()
                .flat_map(|p| p.variants().map(|v| v.to_vec()))
                .collect(),
            cursors: Default::default(),
        });

        if ! provider.expanded_passwords.is_empty() {
            rnp_try!(rnp_ffi_set_pass_provider(
                ffi.as_ptr(),
                Some(password_cb),
                provider.as_mut() as *mut _ as *mut c_void));
        }

        Ok(provider)
    }
}

impl Drop for PasswordProvider {
    fn drop(&mut self) {
        while let Some(p) = self.expanded_passwords.pop() {
            // Securely erase p.
            drop(Password::new(p));
        }
    }
}

/// Constructs an input from a dyn io::Read.
///
/// ATTENTION: Implicit lifetimes!  The returned boxed trait object
/// must outlive the returned rnp_input_t.
unsafe fn rnp_source<'s>(source: &'s mut (dyn io::Read + Send + Sync))
                         -> Result<(rnp_input_t,
                                    Box<dyn io::Read + Send + Sync + 's>)>
{
    let mut source = Box::new(source);
    let mut input = null_mut();
    unsafe extern "C" fn reader(cookie: *mut c_void,
                                buf: *mut c_void,
                                len: size_t,
                                read: *mut size_t)
                               -> bool
    {
        let source: &mut (dyn io::Read + Send + Sync) =
            *(cookie as *mut &mut (dyn io::Read + Send + Sync));
        let buf = std::slice::from_raw_parts_mut(buf as *mut u8, len);
        match source.read(buf) {
            Ok(n) => {
                *read = n;
                true
            },
            Err(_) => false,
        }
    }
    unsafe extern "C" fn closer(_cookie: *mut c_void)
    {
        // Do nothing.
    }
    rnp_try!(rnp_input_from_callback(&mut input as *mut _,
                                     Some(reader),
                                     Some(closer),
                                     source.as_mut()
                                     as *mut &mut (dyn io::Read + Send + Sync)
                                     as *mut _));
    Ok((input, source))
}

/// Constructs an output from a dyn io::Write.
///
/// ATTENTION: Implicit lifetimes!  The returned boxed trait object
/// must outlive the returned rnp_output_t.
unsafe fn rnp_sink<'s>(sink: &'s mut (dyn io::Write + Send + Sync))
                       -> Result<(rnp_output_t,
                                  Box<dyn io::Write + Send + Sync + 's>)>
{
    let mut sink = Box::new(sink);
    let mut output = null_mut();
    unsafe extern "C" fn writer(cookie: *mut c_void,
                                buf: *const c_void,
                                len: size_t)
                                -> bool
    {
        let sink: &mut (dyn io::Write + Send + Sync) =
            *(cookie as *mut &mut (dyn io::Write + Send + Sync));
        let buf = std::slice::from_raw_parts(buf as *const u8, len);
        sink.write_all(buf).is_ok()
    }
    unsafe extern "C" fn closer(_cookie: *mut c_void,
                                _discard: bool)
    {
        // Do nothing.
    }
    rnp_try!(rnp_output_to_callback(&mut output as *mut _,
                                    Some(writer),
                                    Some(closer),
                                    sink.as_mut()
                                    as *mut &mut (dyn io::Write + Send + Sync)
                                    as *mut _));
    Ok((output, sink))
}

/// Constructs an input from a buffer.
///
/// ATTENTION: Implicit lifetimes!  bytes must outlive the returned
/// rnp_input_t.
unsafe fn rnp_input_bytes_ref(bytes: &[u8]) -> Result<rnp_input_t> {
    let mut input = null_mut();
    rnp_try!(rnp_input_from_memory(&mut input as *mut _,
                                   bytes.as_ptr(),
                                   bytes.len(),
                                   false)); // do not copy
    Ok(input)
}

unsafe fn rnp_armor(sink: rnp_output_t, typ: &str) -> Result<rnp_output_t> {
    let mut output = null_mut();
    let typ = CString::new(typ.as_bytes().to_vec()).expect("utf8");
    rnp_try!(rnp_output_to_armor(
        sink,
        &mut output as *mut _,
        typ.as_ptr()));
    Ok(output)
}

unsafe fn _load_keys(ffi: rnp_ffi_t, data: &[u8], flags: u32)
                     -> Result<Vec<rnp_key_handle_t>> {
    let mut handles = Vec::new();
    let mut results_raw: *mut c_char = null_mut();
    {
        let input = rnp_input_bytes_ref(data)?;
        rnp_try!(rnp_import_keys(ffi, input, flags,
                                 &mut results_raw as *mut _));
        rnp_try!(rnp_input_destroy(input));
    }

    #[derive(serde::Deserialize, Debug)]
    struct ImportResults {
        keys: Vec<ImportResult>,
    }

    #[derive(serde::Deserialize, Debug)]
    struct ImportResult {
        #[allow(dead_code)]
        public: String,
        #[allow(dead_code)]
        secret: String,
        fingerprint: String,
    }

    let results: ImportResults =
        serde_json::from_slice(CStr::from_ptr(results_raw).to_bytes())
        .map_err(|e| Error::IoError(io::Error::new(io::ErrorKind::Other, e)))?;
    for result in results.keys {
        let fp = CString::new(result.fingerprint).expect("nul safe");
        let mut handle: rnp_key_handle_t = null_mut();
        rnp_try!(rnp_locate_key(ffi,
                                FINGERPRINT,
                                fp.as_ptr(),
                                &mut handle as *mut _));
        assert!(! handle.is_null());

        let mut is_primary = false;
        rnp_try!(rnp_key_is_primary(handle, &mut is_primary));
        if is_primary {
            handles.push(handle);
        } else {
            rnp_try!(rnp_key_handle_destroy(handle));
        }
    }

    rnp_buffer_destroy(results_raw as *mut _);
    Ok(handles)
}
