use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=rnp");

    let include_path = pkg_config::probe_library("librnp").unwrap()
        .include_paths.pop().unwrap();

    let mut header = include_path.clone();
    header.push("rnp");
    header.push("rnp.h");
    println!("cargo:rerun-if-changed={}", header.to_str().unwrap());

    let bindings = bindgen::Builder::default()
        .clang_arg(format!("-I{}", include_path.display()))
        .header(header.to_str().unwrap())
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .size_t_is_usize(true)
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
